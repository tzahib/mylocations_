

export function setLocalStorageItem(itemName,value){
    localStorage.setItem(itemName,value);
}



export function getLocalStorageItem(itemName){
    var aValue = localStorage.getItem(itemName);
    if(aValue === null){
        aValue = [];
    }
    else
        aValue=JSON.parse(aValue);
    return aValue;
}

