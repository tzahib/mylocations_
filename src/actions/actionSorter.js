import {FETCH_LOCATIONS} from "./categoryActions";
import {LOCAL_STORAGE_LOCATIONS_NAME} from "./categoryActions";
import {getLocalStorageItem} from './index';



export function sortLocationsByParameterName(paramName,isFromAtoZ){
    var aValue=getLocalStorageItem(LOCAL_STORAGE_LOCATIONS_NAME);
    if(isFromAtoZ==true)
        aValue=aValue.sort((item1,item2)=>{
            if(item1[paramName]>item2[paramName])
                return 1;
            else if(item1[paramName]<item2[paramName])
                return -1;
            else
                return 0;
        });
    else
        aValue=aValue.sort((item1,item2)=>{
            if(item1[paramName]>item2[paramName])
                return -1;
            else if(item1[paramName]<item2[paramName])
                return 1;
            else
                return 0;
        });

    var objToReturn={
        data: aValue
    }
    return {
        type: FETCH_LOCATIONS,
        payload: objToReturn
    }
}