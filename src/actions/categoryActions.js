import {getLocalStorageItem,setLocalStorageItem} from './index';

export const FETCH_CATEGORIES='fetch_categories';
export const FETCH_LOCATIONS='fetch_locations';
export const LOCAL_STORAGE_CATEGORIES_NAME='ls_cn';
export const LOCAL_STORAGE_LOCATIONS_NAME='ls_loc';

export function fetchCategories() {
    var aValue=getLocalStorageItem(LOCAL_STORAGE_CATEGORIES_NAME);
    var objToReturn={
        data: aValue
    }
    return {
        type: FETCH_CATEGORIES,
        payload: objToReturn
    }
}


export function addCategories(category) {
    var aValue=getLocalStorageItem(LOCAL_STORAGE_CATEGORIES_NAME);
    aValue.push({category,id: new Date().getTime()});
    setLocalStorageItem(LOCAL_STORAGE_CATEGORIES_NAME,JSON.stringify(aValue));
    var objToReturn={
        data: aValue
    }
    return{
        type: FETCH_CATEGORIES,
        payload: objToReturn
    }
}



export function deleteCategory(item){
    var aValue=getLocalStorageItem(LOCAL_STORAGE_CATEGORIES_NAME);
    aValue=aValue.filter((obj)=>obj.id!=item.id);
    setLocalStorageItem(LOCAL_STORAGE_LOCATIONS_NAME,JSON.stringify(aValue));

    var objToReturn={
        data: aValue
    }
    return {
        type: FETCH_CATEGORIES,
        payload: objToReturn
    }
}

export function updateCategoryItem(item){
var aValue=getLocalStorageItem(LOCAL_STORAGE_CATEGORIES_NAME);
for(var curItem in aValue){
    if(aValue[curItem].id===item.id){
        aValue[curItem].category=item.category;
    }
}
setLocalStorageItem(LOCAL_STORAGE_CATEGORIES_NAME,JSON.stringify(aValue));
    var objToReturn={
        data: aValue
    }
    return {
        type: FETCH_CATEGORIES,
        payload: objToReturn
    }
}
