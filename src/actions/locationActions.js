import {FETCH_LOCATIONS, LOCAL_STORAGE_LOCATIONS_NAME} from "./categoryActions";
import {getLocalStorageItem,setLocalStorageItem} from './index';


export function addLocation(location){
    var aValue=getLocalStorageItem(LOCAL_STORAGE_LOCATIONS_NAME);
    aValue.push(location);
    setLocalStorageItem(LOCAL_STORAGE_LOCATIONS_NAME,JSON.stringify(aValue));

    var objToReturn={
        data: aValue
    }
    return{
        type: FETCH_LOCATIONS,
        payload: objToReturn
    }

}


export function fetchLocations(){
    var aValue=getLocalStorageItem(LOCAL_STORAGE_LOCATIONS_NAME);
    var objToReturn={
        data: aValue
    }
    return {
        type: FETCH_LOCATIONS,
        payload: objToReturn
    }
}

export function filterLocationsByCategory(category){
    var aValue=getLocalStorageItem(LOCAL_STORAGE_LOCATIONS_NAME);
    aValue=aValue.filter((item)=>item.category==category);
    var objToReturn={
        data: aValue
    }
    return {
        type: FETCH_LOCATIONS,
        payload: objToReturn
    }
}

