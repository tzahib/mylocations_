import {FETCH_LOCATIONS} from '../actions/categoryActions';

export default function(state=[],action){
    switch (action.type){
        case(FETCH_LOCATIONS):
            return [action.payload.data,...state];
    }
    return state;
}