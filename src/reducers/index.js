import { combineReducers } from 'redux';
import CategoryReducer from './categories_reducer';
import LocationReducer from './locations_reducer';

const rootReducer = combineReducers({
  categories: CategoryReducer,
  locations: LocationReducer
});

export default rootReducer;
