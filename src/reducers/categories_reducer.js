import {FETCH_CATEGORIES} from '../actions/categoryActions';

export default function(state=[],action){
    switch (action.type){
        case(FETCH_CATEGORIES):
            return [action.payload.data,...state];

    }
    return state;
}