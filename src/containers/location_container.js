import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {fetchLocations,addLocation,filterLocationsByCategory} from "../actions/locationActions";
import {fetchCategories} from "../actions/categoryActions";
import {sortLocationsByParameterName} from "../actions/actionSorter";
import LocationList from "../components/location_list";
import LocationActionsBar from "../components/locations_actionbar";


class LocationContainer extends React.Component {

    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.actions.fetchCategoriesAction();
        this.props.actions.fetchLocationsAction();
    }

    filterLocationByCategoryName(categoryName){
        this.props.actions.filterLocationsByCategory(categoryName);
    }

    sortLocationsByParamName(paramName,isAsc){
        this.props.actions.sortLocationsByParamName(paramName,isAsc);
    }



    render(){

        return (
            <div>
                <LocationActionsBar
                    categoryList={this.props.categories}
                    addNewLocation={this.props.actions.addNewLocation}
                    sortLocationByCategoryName={(catName)=>this.filterLocationByCategoryName(catName)}
                    resetLocationList={()=>this.props.actions.fetchLocationsAction()}

                />
                <LocationList
                    locations={this.props.locations}
                    sortLocationsByParamName={(paramName,isAsc)=>this.sortLocationsByParamName(paramName,isAsc)}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        locations: state.locations,
        categories: state.categories
    };
}

function mapDispatchToProps(dispatch){

    return {
        actions: {
            fetchLocationsAction: bindActionCreators(fetchLocations, dispatch),
            fetchCategoriesAction: bindActionCreators(fetchCategories, dispatch),
            addNewLocation: bindActionCreators(addLocation, dispatch),
            filterLocationsByCategory: bindActionCreators(filterLocationsByCategory,dispatch),
            sortLocationsByParamName: bindActionCreators(sortLocationsByParameterName,dispatch)
        }
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(LocationContainer);
