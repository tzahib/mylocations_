import React from 'react';
import {fetchCategories,addCategories,deleteCategory,updateCategoryItem} from '../actions/categoryActions';


import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import CategoriesList from '../components/categories_list';
import CatagoriesActionsBar from '../components/categories_actionbar';

class CategoryContainer extends React.Component {

    constructor(props){
        super(props);
        this.state={
            categorySelected: ''
        }
    }

    componentWillMount(){
        this.props.actions.fetchCategoriesAction();
    }
    onCategoryAdd(categoryToAdd){
       this.props.actions.addCategory(categoryToAdd);
    }

    onCategoryDelete (){
        this.props.actions.deleteCategory(this.state.categorySelected);
    }
    render (){
        return (
            <div>
                <CatagoriesActionsBar onCategoryAdd={this.onCategoryAdd.bind(this)} onCategoryDelete={this.onCategoryDelete.bind(this)}/>
                <CategoriesList categoriesList={this.props.categories} categorySelected={(itemId)=>{this.setState({categorySelected:itemId})}} updateItem={(item)=>{this.props.actions.updateCategoryItem(item)}} deleteItem={(item)=>this.props.actions.deleteCategory(item)}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { categories: state.categories };
}


function mapDispatchToProps(dispatch){

  return {
        actions: {
            fetchCategoriesAction: bindActionCreators(fetchCategories, dispatch),
            addCategory: bindActionCreators(addCategories, dispatch),
            deleteCategory: bindActionCreators(deleteCategory,dispatch),
            updateCategoryItem: bindActionCreators(updateCategoryItem,dispatch)
        }
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(CategoryContainer);

