import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import {Router,Route} from 'react-router';
import CategoryContainer from './containers/Category_container';
import LocationContainer from './containers/location_container';
import App from './components/app';
import reducers from './reducers';
import createBrowserHistory from 'history/createBrowserHistory';
import ApplicationFooter from './components/app_footer';

const history = createBrowserHistory();

const createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
      <Router history={history}>
          <div>
              <Route path="/" component={App}/>
              <Route path="/categories" component={CategoryContainer}/>
              <Route path="/locations" component={LocationContainer}/>
              <ApplicationFooter/>
          </div>
      </Router>
  </Provider>
  , document.querySelector('.container'));
