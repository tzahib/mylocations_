import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import MapViewer from './google_map_wrapper';

const styles = theme => ({

    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    button: {
        margin: theme.spacing.unit,
        float: 'right',
    },
});

class LocationEditor extends React.Component {

    constructor(props){
        super(props);
        this.state={
            value: 0
        }
    }

    render(){
        const { classes } = this.props;
        if(this.props.item==null)
            return (
                <div></div>
            );
        else
        return (
                <Dialog
                    open={this.props.openDialog}
                    aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Location Editor</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                           View current Location properties
                        </DialogContentText>

                        <Typography component="div" style={{ padding: 8 * 2 }}>
                            <AppBar position="static">
                                <Tabs value={this.state.value}
                                      onChange={(event,value)=>this.setState({value:value})}>
                                    <Tab label="Properties" />
                                    <Tab label="View On Map" />
                                </Tabs>
                            </AppBar>
                        </Typography>

                        {this.state.value === 0 && <TabContainer style={{height:"300px"}}>
                            <span>
                            Name: {this.props.item.name}
                        </span>
                            <br/>
                            <br/>
                            <span>
                            Address: {this.props.item.address}
                        </span>
                            <br/>
                            <br/>
                            <span>
                            Category: {this.props.item.category}
                        </span>
                            <br/>
                            <br/>
                            <span>
                            Coordinates: Lat: {this.props.item.coordinates.lat} Long: {this.props.item.coordinates.long}
                        </span>
                            <br/>
                            <br/>
                            <br/>

                        </TabContainer>}
                        {this.state.value === 1 &&
                        <TabContainer>
                            <MapViewer
                                       lat={this.props.item.coordinates.lat}
                                       long={this.props.item.coordinates.long}
                                       googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                                       loadingElement={<div style={{ height: `100%` }} />}
                                       containerElement={<div style={{ height: `400px` }} />}
                                       mapElement={<div style={{ height: `100%` }} />}></MapViewer>
                        </TabContainer>}

                    </DialogContent>
                    <DialogActions>
                        <Button color="primary" onClick={(ev)=>this.props.closeDialogWindow()}>
                            CLOSE
                        </Button>
                    </DialogActions>
                </Dialog>
        )
    }
}

LocationEditor.propTypes = {
    classes: PropTypes.object.isRequired,
};

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 2 }}>
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};


export default withStyles(styles)(LocationEditor);