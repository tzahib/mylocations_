import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import CategoriesIcon from '@material-ui/icons/List';
import HomeIcon from '@material-ui/icons/Home';
const styles = {
    root: {
        width: 500,
        backgroundColor: 'lightgrey',
        position: 'absolute',
        bottom: '0px',
        width: '100%',
        left: '0px'
    },
};

class ApplicationFooter extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            value: 0,
        };
    }

    handleChange(event, value) {
        this.setState({ value });
        let url="/";
        switch(value){
            case 0:
               url="/";
                break;
            case 1:
                url="/categories";
                break;
            case 2:
                url="/locations";
                break;
        }
        window.location.href=url;
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;

        return (
            <BottomNavigation
                value={value}
                onChange={(ev,value)=>this.handleChange(ev,value)}
                showLabels
                className={classes.root}>
                <BottomNavigationAction label="Home" icon={<HomeIcon />} />
                <BottomNavigationAction label="Categories" icon={<CategoriesIcon />}/>
                <BottomNavigationAction label="Locations" icon={<LocationOnIcon />}  />
            </BottomNavigation>
        );
    }
}

ApplicationFooter.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ApplicationFooter);