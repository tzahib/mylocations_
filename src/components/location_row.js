import React from 'react';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import {withStyles} from "@material-ui/core/styles/index";
import PropTypes from "prop-types";

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
        width: 35,
        height: 10
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
});

class LocationRow extends React.Component {

    constructor(props){
        super(props);
        this.state={
        }
    }

    userClickedOnRow(){
        this.props.updateSelectedItem(this.props.locationItem);
        if (typeof NativeInterface !== "undefined")
            NativeInterface.doVibrate();

        window.location.href='mylocationApp:doVibrate';
    }

    render(){
        let currentItem=this.props.locationItem;
        return (
          <TableRow onClick={(ev)=>this.userClickedOnRow()} style={{cursor:'pointer'}}>
              <TableCell component="th" scope="row" >
                  {currentItem.name}
              </TableCell>
              <TableCell component="th" scope="row" >
                  {currentItem.address}
              </TableCell>
              <TableCell component="th" scope="row" >
                  Lat: {currentItem.coordinates.lat} Long: {currentItem.coordinates.long}
              </TableCell>
              <TableCell component="th" scope="row" >
                  {currentItem.category}
              </TableCell>
          </TableRow>
    );
    }
}

LocationRow.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LocationRow);



