import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    button: {
        margin: theme.spacing.unit,
        float: 'right',
    },
});

class LocationActionsBar extends React.Component {

    constructor(props){
        super(props);
        this.state={
            isAddNewLocationOpened:false,
            filterCategory:'',
            newSelectedCategory:'',
            newSelectedName: '',
            newSelectedAdress:'',
            newSelectedLatitude:'',
            newSelectedLongitude:'',
            errorName:false,
            errorAddress:false,
            errorCoordinate:false,
            errorCategory:false,
            errorLatitude:false,
            errorLongitude:false
        }
    }

    onSubmitNewLocation(){
        let hasError=false;
        if((this.state.newSelectedName.length<1)
        || (this.state.newSelectedCategory.length<1)
        || (this.state.newSelectedAdress.length<1)
        || (!this.validateCoordinations(this.state.newSelectedLatitude,-90,90))
        || (!this.validateCoordinations(this.state.newSelectedLongitude,-180,180))
        )
            hasError=true;

        if(hasError==false){
            let coordObj={lat:this.state.newSelectedLatitude,long:this.state.newSelectedLongitude};
            this.props.addNewLocation({
                'id': new Date().getTime(),
                'name':this.state.newSelectedName,
                'address':this.state.newSelectedAdress,
                'coordinates': coordObj,
                'category':this.state.newSelectedCategory
            });
        }

        this.setState({
            errorName:(this.state.newSelectedName.length<1),
            errorAddress:(this.state.newSelectedAdress.length<1),
            errorCategory:(this.state.newSelectedCategory.length<1),
            errorLatitude: (!this.validateCoordinations(this.state.newSelectedLatitude,-90,90)),
            errorLongitude:(!this.validateCoordinations(this.state.newSelectedLongitude,-180,180)),
            isAddNewLocationOpened: hasError
        });

    }

    validateCoordinations(coord,lowerBound,upperBound){
        try{
        coord=parseFloat(coord);
        if(coord<=upperBound && coord>=lowerBound)
            return true;
        }
        catch(err){}
        return false;
    }

    onFilterChanged(filterName){
        this.setState({ filterCategory: filterName });
        this.props.sortLocationByCategoryName(filterName);
    }

    render(){
        const { classes } = this.props;
        if(typeof this.props.categoryList[0]  == "undefined")
            return (
                <div>
                    Please Wait...
                </div>
            )
        else
            return (
            <div>
                Filter locations by selecting category
                <FormControl className={classes.formControl} style={{marginLeft:10}}>
                    <InputLabel htmlFor="controlled-open-select">Select Filter</InputLabel>
                    <Select
                        required
                        value={this.state.filterCategory}
                        onChange={(event)=>this.onFilterChanged(event.target.value)}
                        inputProps={{
                            name: 'Category' ,
                            id: 'cat-filter',
                        }}

                    >
                        {this.props.categoryList[0].map(item => {
                            return (
                                <MenuItem key={item.id} value={item.category}>
                                    {item.category}
                                </MenuItem>
                            )
                        })
                        }
                        }
                    </Select>
                </FormControl>

                <Button onClick={()=>this.props.resetLocationList()} color="primary" >
                    RESET
                </Button>

                <Button variant="raised" size="medium" color="primary" className={classes.button} onClick={()=>this.setState({isAddNewLocationOpened:true})} >
                    Add Location
                </Button>
                <Dialog
                    open={this.state.isAddNewLocationOpened}
                    onClose={this.isAddNewLocationOpened}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Add New Location</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To add new location please fill-in the following
                        </DialogContentText>
                        <TextField
                            required
                            error={this.state.errorName}
                            margin="dense"
                            id="name"
                            label="Name"
                            value={this.state.newSelectedName}
                            onChange={(ev)=>this.setState({newSelectedName:ev.target.value,errorName:false})}
                            fullWidth
                        />
                        <TextField
                            required
                            margin="dense"
                            error={this.state.errorAddress}
                            id="address"
                            label="Address"
                            value={this.state.newSelectedAdress}
                            onChange={(ev)=>this.setState({newSelectedAdress:ev.target.value,errorAddress:false})}
                            fullWidth
                        />
                        <div id="coordinationContainer">
                            <span>Coordination:  </span>
                            <TextField
                                required
                                error={this.state.errorLatitude}
                                margin="dense"
                                id="lat"
                                label="Latitude"
                                value={this.state.newSelectedLatitude}
                                onChange={(ev)=>this.setState({newSelectedLatitude:ev.target.value,errorLatitude:false})}
                            />
                            <TextField
                                required
                                error={this.state.errorLongitude}
                                margin="dense"
                                id="lat"
                                label="Longitude"
                                value={this.state.newSelectedLongitude}
                                onChange={(ev)=>this.setState({newSelectedLongitude:ev.target.value,errorLongitude:false})}
                            />
                        </div>


                        <FormControl className={classes.formControl} style={{marginLeft:0}}>
                            <InputLabel htmlFor="controlled-open-select">{this.state.errorCategory ? ('⚠️ Error'): 'Category'}</InputLabel>
                        <Select
                            required
                            value={this.state.newSelectedCategory}
                            onChange={(event)=>this.setState({ newSelectedCategory: event.target.value })}
                            inputProps={{
                                name: 'Category' ,
                                id: 'cat-simple',
                            }}

                        >
                            {this.props.categoryList[0].map(item => {
                                    return (
                                        <MenuItem key={item.id} value={item.category}>
                                            {item.category}
                                        </MenuItem>
                                    )
                                })
                            }
                            }
                        </Select>
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary" onClick={()=>this.setState({isAddNewLocationOpened:false,newSelectedCategory:''})}>
                            Cancel
                        </Button>
                        <Button onClick={this.handleClose} color="primary" onClick={(event)=>this.onSubmitNewLocation()}>
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
            )
    }
}

export default withStyles(styles)(LocationActionsBar);
