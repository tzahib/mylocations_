import React,{Component} from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CategoryRow from './categoryRow'


    export default function CategoriesList(props) {

        let catList=props.categoriesList;
        if(typeof catList=="undefined" || catList.length<1){
            return (
                <div>
                    No Available Categories
                </div>
            )
        }
        return(
            <Paper >
                <Table >
                    <TableHead>
                        <TableRow>
                            <TableCell>Category Name</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {catList[0].map(item => {
                        return (
                            <CategoryRow key={item.id}
                                         item={item}
                                         onValueChanged={(item)=>props.updateItem(item)}
                                         onItemRemove={(item)=>props.deleteItem(item)}>
                            </CategoryRow>
                        );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        )
    }

