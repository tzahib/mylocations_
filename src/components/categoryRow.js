import React,{Component} from 'react';
import PropTypes from 'prop-types';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import DelIcon from '@material-ui/icons/DeleteForever';
import DoneIcon from '@material-ui/icons/Done';
import Icon from '@material-ui/core/Icon';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
        width: 35,
        height: 10
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    menu: {
        width: 200,
    },
});

class CategoryRow extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            isSelected: false,
            editMode: false,
            changedValue: this.props.item.category
        };
    }
    onTableRowSelected(event,currentItem){

    }
    submitChanges(event){
        event.preventDefault();
        this.props.onValueChanged({id: this.props.item.id,category:this.state.changedValue});
        this.setState({
            editMode: false
        });
    }

    render(){
        const { classes } = this.props;
        let currentItem=this.props.item;
        return (
            <TableRow
                hover
                onClick={event => this.onTableRowSelected(event,currentItem)}
                selected={this.state.isSelected}>
                <TableCell component="th" scope="row" >
                    <Button
                        variant="fab"
                        color="primary"
                        aria-label="modift"
                        className={classes.button}
                        onClick={()=>this.setState({editMode:!this.state.editMode})}>
                        <Icon>edit_icon</Icon>
                    </Button>

                    <Button variant="fab" aria-label="delete"  className={classes.button} onClick={()=>this.props.onItemRemove(currentItem)}>
                        <DelIcon />
                    </Button>

                    <div style={{
                            bottom: '25px',
                            position: 'relative',
                            left: '127px'}}>
                        <span style={{display: this.state.editMode==false ? "block" : "none"}}>
                            {currentItem.category}
                        </span>
                        <form className={classes.container}
                              noValidate
                              autoComplete="off"
                              style={{
                                width: '100%',
                                position: 'relative',
                                top: '-14px',
                                display: this.state.editMode==true ? "block" : "none"}}
                              onSubmit={(event)=>this.submitChanges(event)}>
                        <TextField
                            fullWidth
                            className={classes.textField}
                            value={this.state.changedValue}
                            onChange={(ev)=>this.setState({changedValue: ev.target.value})}
                            margin="normal"
                        />
                            <Button
                                variant="fab"
                                aria-label="Save Changes"
                                color="primary"
                                className={classes.button}
                                onClick={(event)=>this.submitChanges(event)}>
                                <DoneIcon />
                            </Button>
                        </form>
                    </div>
                </TableCell>
            </TableRow>
        );
    }
}

CategoryRow.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CategoryRow);
