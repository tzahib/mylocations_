import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    actionBarStyles:{
        'width':'100%',
        'paddingLeft': '4px',
        'paddingBottom': '22px'
    }
});

export default class CategoriesActions extends React.Component {

    constructor(props){
        super(props);
        this.state={
            currentNewCategory: ''
        }
    }

    render(){
        return(
          <div style={styles.actionBarStyles}>
              <div>
                  <TextField
                      id="categoryAddInputField"
                      label="Add Category"
                      placeholder="Category"
                      margin="normal"
                      onChange={(event)=>{this.setState({currentNewCategory:event.target.value})}}
                      fullWidth
                      style={{width:'95%'}}
                  />
                  <Button
                      variant="fab"
                      color="primary"
                      aria-label="add"
                      onClick={()=>{this.props.onCategoryAdd(this.state.currentNewCategory)}}>
                      <AddIcon />
                  </Button>
              </div>
          </div>
        );
    }
}