import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import LocationRow from './location_row';
import LocationEditor from './location_editor';


const styles = {
    tableCell:{
        cursor:'pointer'
    }
}
export default class LocationList extends React.Component{

    constructor(props){
        super(props);
        this.state={
            showDialog:false,
            selectedItem:null,
            sortAsc:true,
        }
    }

    sortTableByParameter(paramName){
        this.props.sortLocationsByParamName(paramName,this.state.sortAsc);
        this.setState({
            sortAsc: !this.state.sortAsc
        })
    }



    render() {
        let locationsList = this.props.locations;
        if (typeof locationsList == "undefined" || locationsList.length < 1) {
            return (
                <div>
                    <br/>
                    <br/>
                    No Available Locations
                </div>
            )
        }
        return (
            <Paper>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell onClick={()=>this.sortTableByParameter("name")} style={styles.tableCell}>Name</TableCell>
                            <TableCell onClick={()=>this.sortTableByParameter("address")} style={styles.tableCell}>Address</TableCell>
                            <TableCell style={{cursor:'default'}}>Coordinates</TableCell>
                            <TableCell onClick={()=>this.sortTableByParameter("category")}style={styles.tableCell}>Category</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            locationsList[0].map(item => {
                                return (
                                    <LocationRow
                                        locationItem={item}
                                        key={item.id}
                                        updateSelectedItem={(item)=>this.setState({selectedItem:item,showDialog:true})}/>
                                );
                            })}
                    </TableBody>
                </Table>
                <LocationEditor
                    openDialog={this.state.showDialog}
                    item={this.state.selectedItem}
                    closeDialogWindow={()=>this.setState({showDialog:false})}/>
            </Paper>
        )
    }
}

