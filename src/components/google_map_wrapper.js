import React from 'react';
import { GoogleMap,withGoogleMap,withScriptjs,Marker } from 'react-google-maps';

const MapViewer = withScriptjs(withGoogleMap((props) =>
    <GoogleMap
        defaultZoom={8}
        defaultCenter={{ lat: parseFloat('1'), lng: parseFloat('1') }}
    >
     <Marker position={{ lat: parseFloat('1'), lng: parseFloat('1')  }} />
    </GoogleMap>
));


export default MapViewer;