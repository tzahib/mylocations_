# My Location application

This application manage locations and categories

#### Familiar with Git?
Checkout this repo, install dependencies, then start the npm process with the following:
 
 
```
> npm install
> npm start
```

#### Native Support
When user clicks on a row in the locations table it triggers Vibration of the device in the native code.
In order to make it work, you will need to use webView(android and iOS) to load the Application and create native handling.

##### Android
NativeInterface.doVibrate();

###### in android native application add the following
```
    @SuppressLint("JavascriptInterface")
    @JavascriptInterface
    public void doVibrate(){
        Vibrator v = (Vibrator) this.myContext.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        }
        else
            v.vibrate(500);
    }
```

##### iOS
window.location.href='mylocationApp:doVibrate';

###### in iOS add the following code to the native iOS appliaction
```
- (BOOL)webView:(UIWebView *)webView2
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType {
    
    // Intercept custom location change, URL begins with "js-call:"
    if ([[[request URL] absoluteString] hasPrefix:@"mylocationApp:"]) {
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
```
